<?php

namespace SteeveDroz\CiAuth;

interface UserModelInterface
{
    public function login(array $user): ?array;
}
