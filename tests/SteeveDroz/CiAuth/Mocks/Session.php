<?php

namespace SteeveDroz\CiAuth\Mocks;

class Session
{
    private static $INSTANCE = null;

    private function __construct()
    {
        \session_start();
    }

    public static function getInstance()
    {
        if (null === static::$INSTANCE) {
            static::$INSTANCE = new Session();
        }

        return static::$INSTANCE;
    }

    public function set($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    public function get($key)
    {
        return $_SESSION[$key] ?? null;
    }

    public function remove($key)
    {
        $_SESSION[$key] = null;
    }
}
