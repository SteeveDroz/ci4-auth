<?php

namespace SteeveDroz\CiAuth\Mocks;

use SteeveDroz\CiAuth\UserModelInterface;

class UserModelValid implements UserModelInterface
{
    public function login(array $user): ?array
    {
        return $user;
    }
}
