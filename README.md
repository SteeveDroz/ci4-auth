# CodeIgniter 4 Authentication Library

![Pipeline](https://gitlab.com/SteeveDroz/ci4-auth/badges/master/pipeline.svg) ![Coverage](https://gitlab.com/SteeveDroz/ci4-auth/badges/master/coverage.svg)

When Using CodeIgniter 4, you sometimes need to authenticate a user. This library allows to very easily:

- Log in a user if their credentials correspond to your model requirement
- Check if a user is logged in
- Get the user that is logged in
- Get an attribute of the logged in user
- Log out the user

It can be used in controllers, but also in views, models, or even in libraries or helpers.

## Set up

To install ci4-auth, simply run `composer require steevedroz/ci4-auth` in your CodeIgniter 4 root repository.

In order to use it correctly, here are the steps you should follow:

1. Make a `UserModel` (that you can call any name) and make it implement `SteeveDroz\CiAuth\UserModelInterface`.
1. Add a public method with the signature `login(array $user): ?array`. That method should take an `array` with credentials (such as username and password) and return the corresponding full user, or `null` if the credentials don't match.
1. Optionnally, create a service called `auth` in `app\Config\Services.php` by following the [instructions on the official website](https://codeigniter4.github.io/userguide/concepts/services.html). Be careful, to call the constructor, you must pass your user model as parameter (see below).
1. If using the parser to render views, feel free to [create a plugin](https://codeigniter4.github.io/userguide/outgoing/view_parser.html#registering-a-plugin) with `'getUser' => 'SteeveDroz\CiAuth\Auth::getUserStatic'`. The previous step (i.e. create a service) is not optionnal anymore if you use this feature as `getUserStatic` uses the `auth` service.

## Usage

After set up, you can access the authentication library with `service('auth')` if you set up the service or by calling `new \SteeveDroz\CiAuth\Auth(new UserModel())`, `UserModel` being the one you created in your models.

If you want to keep the original user in the session and not update it with modification from the database on each page, add a second parameter to the constructor and set it to `false`.

Once you have a variable (or the service) that contains an instance of `Auth`, you can use the methods below.

## Class reference

### `SteeveDroz\CiAuth\Auth`

#### `__construct($model, $refresh = true)`

##### Parameters

- **$model** (*SteeveDroz\CiAuth\UserModelInterface*) The model that will check the credentials
- **$refresh** (*bool*) If the user is updated on each page

Creates an instance of the authentication library.

#### `login($user)`

##### Parameters

- **$user** (*array*) The user containing the credentials that need checking.

Checks the credentials with the model. If they are correct, saves the `array` to the session under the `user` key.

##### Return type

*void*

#### `logout()`

Removes the logged in user from the session.

##### Return type

*void*

#### `isLoggedIn()`

Checks if a user is currently logged in.

##### Returns

`true` if a user was found in the session and `false` otherwise.

##### Return type

*bool*

#### `getUser([$field])`

##### Parameters

- **$field** (*string*) The field one wants to retrieve from the user stored in session

If no parameters is given, fetches the user in session and returns it. If a parameter is given, only returns the corresponding attribute in the user in session; for instance, `$auth->getUser('firstName')` returns `$user['firstName']`.

##### Returns

The user as an `array`, or one of its attributes, or `null` if nothing matches.

##### Return type

*mixed*

#### `getUserStatic([$field])`

##### Parameters

- **$field** (*array*) An array with, as first element, the field one wants to retrieve from the user stored in session

If no parameters is given, fetches the user in session and returns it. If a parameter is given, only returns the corresponding attribute in the user in session; for instance, `Auth::getUserStatic(['firstName'])` returns `$user['firstName']`.

This static method is inteded to be used with a parser plugin. For example, if the `getUser` plugin is created as the example above, it could be used this way: `{+getUser email+}`. The plugin calls the method by passing all the keywords as an array, hence the paramter type.

##### Returns

The user as an `array`, or one of its attributes, or `null` if nothing matches.

##### Return type

*mixed*
